////////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Jaeden Chang <jaedench@hawaii.edu>
/// @date    23_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_MAX_NUMBER 2048 //define global constant

int main( int argc, char* argv[] ) {
   int theMaxValue = DEFAULT_MAX_NUMBER; //initialize max num as default
   if (argc > 1){ //>1 means that a command line parameter is being passed
      theMaxValue = atoi(argv[1]); //set max num to parameter
         if (theMaxValue < 1){ //valid parameter is >=1
            printf("You must enter a number that's >= 1.\n"); // error message
            exit(1); //exit program, returning a 1 to the command line
         }
      }

   srand(time(0)); //produces different sequence of random numbers on every program run
      int theNumberImThinkingOf = (rand() % theMaxValue) + 1; //the number the cat is thinking of
                                                        ////this eq. will produce numbers less than theMaxValue
      //printf("%d\n", theNumberImThinkingOf); //temp print of the num cat is thinking of 
 
       int aGuess = 0;

   // do statements while condition
   // do while loop for initial guess
   do {
 //     int aGuess = 0;

      printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess: \n", theMaxValue);
      scanf("%d", &aGuess);

      if (aGuess < 1) {
         printf("You must enter a number that's >= 1\n");
         continue;
      }

      if (aGuess > theMaxValue) {
         printf("You must enter a number that's <= %d\n", theMaxValue);
         continue;
      }

      if (aGuess > theNumberImThinkingOf) {
         printf("No cat...the number I'm thinking of is smaller than %d\n", aGuess);
         continue;
      }

      if (aGuess < theNumberImThinkingOf) {
         printf("No cat...the number I'm thinking of is larger than %d\n", aGuess);
         continue;
      }

   } while ( aGuess != theNumberImThinkingOf );

   // do while loop will exit when aGuess = theNumberImThinkingOf
   // could include an if statement
   printf("You got me.\n");
   printf(" /\\_/\\\n");     
   printf("( o.o )\n");
   printf(" > ^ <\n");
   return 0;
}

   

   //GIVEN EXAMPLE CODE
   //printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc );
   

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   //int aGuess;
   //printf( "Enter a number: " );
   //scanf( "%d", &aGuess );
   //printf( "The number was [%d]\n", aGuess );

//return 1;  // This is an example of how to return a 1


